#!/usr/bin/python3

import math;

def numStages(width,maxWidth):
    stages=0
    high = width
    while(high > 0):
        high = high - maxWidth
        stages = stages + 1
    return stages

def stageRange(width,maxWidth,stage):
    high = (width-(stage*maxWidth))-1
    if(high>0):
        low = max(high-maxWidth+1,0)
        return [high,low]


# this needs work as [128,128,100..] does not
# calculate correctly
def calcPrestages(stage,maxWidth):
  return math.floor((stages[0]-1)/maxWidth)

# inputs
stages = [156,130,100,80,40]
maxWidth = 64

prestages = calcPrestages(stages[0],maxWidth)

print("library ieee;")
print("use ieee.std_logic_1164.all;")
print("use ieee.numeric_std.all;")

print("\r")

entityName = ("cicIntegrator_%u_%u"%(stages[0],stages[-1]))

print("entity %s is" % entityName);
print("\tport (")
print("\tclk : in std_logic;")
print("\tsrst : in std_logic;")
print("\tdata : in signed(%u downto 0);" % (stages[0]-1))
print("\tq    : in signed(%u downto 0)" % (stages[-1]-1))
print(");");
print("end;\n");

print("-- stages=",stages,"; maxWidth = %u\n"%maxWidth)
print("architecture behav of %s is" % entityName)

nregs=0
pnregs=0


for s in range(-prestages,len(stages)):
  if(s<0):
    # print("\t-- prestage %u",-s);
    for p in range((-s)):
      high = stages[0] - (maxWidth*p) - 1;
      low = max(high - maxWidth + 1, 0)
      print("\tsignal b%up%u : signed(%u downto %u); -- %ubits"%\
        (-s,p,high,low,high-low+1))
      pnregs = pnregs + (high-low-1)
  else:
    # print("\t-- stage%u"%s);
    for p in range(math.ceil(stages[s]/maxWidth)):
      high=stages[s] - (maxWidth*p) - 1 
      low = max(high - maxWidth + 1, 0)
      high = high + (1 if p>0 else 0);
      print("\tsignal s%up%u : signed(%u downto %u); -- %ubits%s"%\
        (s,p,high,low,high-low+1,\
          "*" if p>0 else ""))
      nregs = nregs + (high-low-1)

print("\t-- delay = %3u cycles" % (len(stages)+prestages+1))
print("\t-- usage = %u luts (%u as buffers)" % (nregs+pnregs,pnregs))

print("begin\n");

print("process(clk)\nbegin\n\tif rising_edge(clk) then")


for s in range(-prestages,len(stages)):
  print();
  if(s<0):
    for p in range((-s)):
      # print("\t-- prestage %u %u"%(-s,p));
      high = stages[0] - (maxWidth*p) - 1;
      low = max(high - maxWidth + 1, 0)
      if (-s)==prestages:
        src="data"
      else:
        src="b%up%u"%((-s)+1,p)

      print("\t\tb%up%u(%u downto %u) \t<= %4s(%u downto %u);"%\
        (-s,p,high,low,src,high,low))
        
  elif s==0:
    maxp = math.ceil(stages[s]/maxWidth)
    for p in range(maxp):
      high=stages[0] - (maxWidth*p) - 1;
      low = max(high - maxWidth + 1, 0)

      phigh=stages[0] - (maxWidth*p) - 1;
      plow = max(phigh - maxWidth + 1, 0)

      print("\t\t%s \t<= %s + %s %s;"%(\
        ("s%up%u(%u downto %u)"%(s,p,high+(1 if p>0 else 0),low)),\
        ("%ss%up%u(%u downto %u)"%("\"0\"&" if p>0 else "",s,p,high,low)),\
        ("data(%u downto %d)"%(high,low) if (p==prestages) else "b%up%d(%u downto %u)"%((-s)+p+1,p,phigh,plow)), \
        ("\t + s%up%u(%u downto %u)"%(s,p+1,low,low) if (p<(maxp-1)) else "")))

  else:
    # print("\t-- stage%u"%s);
    maxp = math.ceil(stages[s]/maxWidth)
    for p in range(maxp):
      high=stages[s] - (maxWidth*p) - 1;
      low = max(high - maxWidth + 1, 0)
      r = (high-low)+1

      phigh=stages[s-1] - (maxWidth*p) - 1;
      plow = phigh-(high-low)

      print("\t\t%s(%u downto %u) \t<= %s(%u downto %u) + %s(%u downto %u)%s; --%ubits"%(\
        ("s%up%u"%(s,p)),high+(1 if p>0 else 0),low,\
        ("%ss%up%u"%("\"0\"&" if p>0 else "",s,p)),high,low,\
        ("s%up%d"%(s-1,p)),phigh,plow,\
        ("\t + s%up%u(%u downto %d)"%(s,p+1,low,low) if (p<(maxp-1)) else ""),r))

print("\t\tif srst='1' then");
for s in range(-prestages,len(stages)):
  if(s<0):
    # print("\t-- prestage %u",-s);
    for p in range((-s)):
      high = stages[0] - (maxWidth*p) - 1;
      low = max(high - maxWidth + 1, 0)
      print("\t\t\tb%up%u <= (others => '0');"%(-s,p))
  else:
    # print("\t-- stage%u"%s);
    for p in range(math.ceil(stages[s]/maxWidth)):
      high=stages[s] - (maxWidth*p) - 1 
      low = max(high - maxWidth + 1, 0)
      high = high + (1 if p>0 else 0);
      print("\t\t\ts%up%u <= (others => '0');"%(s,p))
print("\t\tend if;");


print("\tend if;\nend process;")

# for i in range(len(stages)):
#   if(a)
print("\nend architecture;");


