
### CIC-Tools

This is a collection of tools to generate CIC filters.

> [read here](https://dspguru.com/files/cic.pdf) for explanation of cic filters. There are many people more competent at explaining the concepts -so lets not reinvent the wheel. This following should be seen as a quick guide to the overall architecture.

CIC Filters (Hogenauer) are an effective filter used in the classical digital frontends for since the early 90's, such as the ![GrayChip](docs/GC4016.pdf)4016 and it's variants. They are ideally suited to ASIC and particularly FPGA's as they contain no multipliers in the CIC filter itself.  

A classic digitial downconverter looks like this :-  
![ddc](ddc.svg)  
In the above configuration, the CIC has variable decimation which sets the output sample rate. The C/FIR and P/FIR each have a decimation of 2 so the output sample rate is SampleRate/(CIC-dec*4)
|Sample Rate|CIC Decimation|Overal Decimation|Output Sample Rate|
|-----|-----|----|----|
|120MSPS|6|24|5MSPS|
|120MSPS|60|240|500kSPS|

There are three parts to a receiver CIC filter - an n-stage Integrator section, followed by a Decimator, followed by a n-stage Comb section.  
![cic](cic-5stage.png)  
The Integrator filter works at full rate is needs to be pipelined, however after decimation if the decimation is equal or greater than the number of stages, the Comb can be done sequentially. For example, the GC4016 has a 5 stage Integrator, but a min decimation of 4 so the Comb works at full rate.  

Where an adder size gets too large, as may be the case with a sounder where decimation rates are quite high, the width of the first adder stages may be problematic to achieve timing. For this case, buildcic.py was written to create a minimal logic vhdl file capable of pipelining the integrator state.  

In this example, a five stage integrator of sizes [156,130,100,80,40] with a maximum width of 64bits (plus one bit for carry) is shown.  
![integrator](docs/integrate_156_32.svg) 

